package main.java.ca.dal.cs.csci3130.tddci;

public class RomanConverter {

	public int convertRomanToArabicNumber(String romanNumber) {

		int sum = 0;
		if (romanNumber.indexOf('V') >= 0) {
			int vIndex = romanNumber.indexOf('V');
			int sumPrev = 0;
			for (int i = 0; i < vIndex; i++) {
				char ch = romanNumber.charAt(i);
				if (ch == 'I') {
					sumPrev += 1;
				} else {
					throw new IllegalArgumentException();
				}
			}
			sum = 5 - sumPrev;

		} else {
			for (char ch : romanNumber.toCharArray()) {
				if (ch == 'I') {
					sum += 1;
				} else {
					throw new IllegalArgumentException();
				}
			}
		}

		return sum;
	}

}
