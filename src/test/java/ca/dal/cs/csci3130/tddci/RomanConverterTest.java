package test.java.ca.dal.cs.csci3130.tddci;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import main.java.ca.dal.cs.csci3130.tddci.RomanConverter;

public class RomanConverterTest {

	@Test
	public void convertI() {
		assertEquals(1, new RomanConverter().convertRomanToArabicNumber("I"));
	}

	@Test
	public void convertII() {
		assertEquals(2, new RomanConverter().convertRomanToArabicNumber("II"));
	}

	@Test
	public void convertIII() {
		assertEquals(3, new RomanConverter().convertRomanToArabicNumber("III"));
	}

	
	  @Test public void convertIV() { assertEquals(4, new
	  RomanConverter().convertRomanToArabicNumber("IV")); }
	 
	  
	  @Test public void convertV() { assertEquals(5, new
	  RomanConverter().convertRomanToArabicNumber("V")); }
	 
}
